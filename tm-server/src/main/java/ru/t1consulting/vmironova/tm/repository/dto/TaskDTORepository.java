package ru.t1consulting.vmironova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1consulting.vmironova.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> implements ITaskDTORepository {

    public TaskDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public TaskDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final TaskDTO task = new TaskDTO(name, description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public TaskDTO create(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final TaskDTO task = new TaskDTO(name);
        return add(userId, task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}
