package ru.t1consulting.vmironova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.api.repository.model.IProjectRepository;
import ru.t1consulting.vmironova.tm.model.Project;

import javax.persistence.EntityManager;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final Project project = new Project(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws Exception {
        @NotNull final Project project = new Project(name);
        return add(userId, project);
    }

}
