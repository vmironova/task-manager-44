package ru.t1consulting.vmironova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.vmironova.tm.api.repository.dto.IUserDTORepository;
import ru.t1consulting.vmironova.tm.api.service.IConnectionService;
import ru.t1consulting.vmironova.tm.api.service.IPropertyService;
import ru.t1consulting.vmironova.tm.api.service.dto.IProjectDTOService;
import ru.t1consulting.vmironova.tm.api.service.dto.ITaskDTOService;
import ru.t1consulting.vmironova.tm.api.service.dto.IUserDTOService;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.marker.UnitCategory;
import ru.t1consulting.vmironova.tm.repository.dto.UserDTORepository;
import ru.t1consulting.vmironova.tm.service.ConnectionService;
import ru.t1consulting.vmironova.tm.service.PropertyService;
import ru.t1consulting.vmironova.tm.service.dto.ProjectDTOService;
import ru.t1consulting.vmironova.tm.service.dto.TaskDTOService;
import ru.t1consulting.vmironova.tm.service.dto.UserDTOService;
import ru.t1consulting.vmironova.tm.util.HashUtil;

import javax.persistence.EntityManager;

import static ru.t1consulting.vmironova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserDTORepositoryTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService);

    @NotNull
    private static IUserDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserDTORepository(entityManager);
    }

    @NotNull
    private static EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = userService.add(USER_TEST);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = userService.findByLogin(USER_TEST_LOGIN);
        if (user != null) userService.remove(user);
    }

    @After
    public void after() throws Exception {
        @Nullable UserDTO user = userService.findOneById(ADMIN_TEST.getId());
        if (user != null) userService.remove(user);
    }

    @Test
    public void add() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(ADMIN_TEST));
            entityManager.getTransaction().commit();
            @Nullable final UserDTO user = repository.findOneById(ADMIN_TEST.getId());
            Assert.assertNotNull(user);
            Assert.assertEquals(ADMIN_TEST.getId(), user.getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void existsById() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId()));
        entityManager.close();
    }

    @Test
    public void findOneById() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final UserDTO user = repository.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
        entityManager.close();
    }

    @Test
    public void removeByUserId() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(ADMIN_TEST);
            Assert.assertNotNull(repository.findOneById(ADMIN_TEST.getId()));
            repository.remove(ADMIN_TEST);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void create() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD));
            Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
            Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createWithEmail() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD), ADMIN_TEST_EMAIL);
            Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
            Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
            Assert.assertEquals(ADMIN_TEST.getEmail(), user.getEmail());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createWithRole() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = repository.create(ADMIN_TEST_LOGIN, HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD), Role.ADMIN);
            Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
            Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
            Assert.assertEquals(Role.ADMIN, user.getRole());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findByLogin() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        @Nullable final UserDTO user = repository.findByLogin(USER_TEST.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
        entityManager.close();
    }

    @Test
    public void findByEmail() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        @Nullable final UserDTO user = repository.findByEmail(USER_TEST.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
        entityManager.close();
    }

    @Test
    public void isLoginExists() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        Assert.assertTrue(repository.isLoginExists(USER_TEST.getLogin()));
        entityManager.close();
    }

    @Test
    public void isEmailExists() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        Assert.assertTrue(repository.isEmailExists(USER_TEST.getEmail()));
        entityManager.close();
    }

}
