package ru.t1consulting.vmironova.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.vmironova.tm.exception.AbstractException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        try {
            @NotNull final String value = nextLine();
            return Integer.parseInt(value);
        } catch (@NotNull final RuntimeException e) {
            throw new NumberInvalidException();
        }
    }

    final class NumberInvalidException extends AbstractException {
        public NumberInvalidException() {
            super("Error! Value is not a number.");
        }
    }

}
